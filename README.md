# Country Government Chooser
A simple Country Government chooser, written in JAVA, to demonstrate choosing a government type for a grid of countries.
## Overview
The code includes examples of:
* JOptionPane - for dialogs
* Grid Layout - for UI design
* JFrame and JPanel - for basic UI components
* JButton - specialized to handle choice of government
* enumeration - available government types, which mouse button was clicked
* ## Tools Used

| Tool     |  Version |
|:---------|---------:|
| Java     | 23.0.0.0 |
| IntelliJ | 2024.2.3 |

## References
* [How to Make Dialogs with JOptionPane](https://docs.oracle.com/javase/tutorial/uiswing/components/dialog.html)
